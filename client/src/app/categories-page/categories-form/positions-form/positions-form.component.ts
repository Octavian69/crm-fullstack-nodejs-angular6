import { Component, OnInit, Input, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { PositionService } from '../../../shared/services/position.service';
import { Position, Message } from '../../../shared/intefaces';
import { MaterialService, MaterialInstance } from '../../../shared/classes/material.service';
import { delay, min } from '../../../../../node_modules/rxjs/operators';
import { FormGroup, FormControl, Validators } from '../../../../../node_modules/@angular/forms';


@Component({
  selector: 'app-positions-form',
  templateUrl: './positions-form.component.html',
  styleUrls: ['./positions-form.component.css']
})
export class PositionsFormComponent implements OnInit, AfterViewInit, OnDestroy {

  constructor(
    private positionService: PositionService
  ) { }

  @Input('categoryId') categoryId: string = '';
  @ViewChild('modalRef') modalRef: ElementRef;

  form: FormGroup;
  modal: MaterialInstance;
  positions: Position[] = [];
  isLoad: boolean = false;
  positionId: string | null = null;

  ngOnInit() {
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required]),
      cost: new FormControl(1, [Validators.required, Validators.min(1)])
    })

    this.positionService.fetch(this.categoryId).pipe(delay(500))
      .subscribe(
        (positions: Position[]) => {
           this.positions = positions;
           this.isLoad = true;
          
        },
        error => MaterialService.toast(error.error.message)
      )
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  ngOnDestroy() {
    this.modal.destroy();
  }

  private resetForm(): void {
    this.form.reset({
      name: null,
      cost: 1
    })
  }

  private onAddPosition() {
    this.resetForm();
    this.positionId = null;
    this.modal.open();
    MaterialService.updateTextFields();
  }

  private onClose() {
    this.modal.close();

    this.resetForm();
  }

  private onDeletePosition(e: Event, position: Position) {
    e.stopPropagation();

    const decision = confirm(`Вы действительно хотите удалить позицию "${position.name}" ?`);
    
    if(decision) {
      this.positionService.deletePosition(position._id)
      .subscribe((response) => {
        MaterialService.toast(response.message);
        const idx = this.positions.findIndex(p => p._id === position._id);
        this.positions.splice(idx, 1);
      })
    }
  }

  private onSelectPosition(position: Position) {
    
    this.modal.open();
    this.positionId = position._id;

    this.form.patchValue({
      name: position.name,
      cost: position.cost
    });

    MaterialService.updateTextFields();
  }

  onSubmit() {
    this.form.disable();

    const { name, cost } = this.form.value;
    const newPosition: Position = {
      name,
      cost,
      category: this.categoryId
    }

    const completed = () => {
      this.form.enable();
      this.modal.close();
      this.resetForm();
    }

    if(this.positionId) {
      newPosition._id = this.positionId;

      this.positionService.updatePosition(newPosition)
      .subscribe(
        (position: Position) => {
          const idx = this.positions.findIndex(p => p._id === position._id);
          this.positions[idx] = position;
          MaterialService.toast('Позиция успешно отредактированна.');
        },
        error => MaterialService.toast(error.error.message),
        completed
      )
    } else {
      this.positionService.createPosition(newPosition)
      .subscribe(
        (position: Position) => {
          this.positions.push(position);
          MaterialService.toast('Позиция успешно добавлена');
        },
        error => MaterialService.toast(error.error.message),
        completed
      )
    }
  }
}
