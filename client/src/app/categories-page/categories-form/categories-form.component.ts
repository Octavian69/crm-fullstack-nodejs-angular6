import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { FormGroup, FormControl, Validators } from '../../../../node_modules/@angular/forms';
import { mergeMap, delay } from '../../../../node_modules/rxjs/operators';
import { of, Observable } from '../../../../node_modules/rxjs';

import { MaterialService } from '../../shared/classes/material.service';
import { Category, Message } from '../../shared/intefaces';
import { CategoriesService } from '../../shared/services/categories.service';


@Component({
  selector: 'app-categories-form',
  templateUrl: './categories-form.component.html',
  styleUrls: ['./categories-form.component.css']
})
export class CategoriesFormComponent implements OnInit {

  private form: FormGroup;
  private image: File;
  private category: Category;
  private isNew: boolean = true;
  private imagePreview: string = '';
  @ViewChild('fileInput') fileInput: ElementRef;

  constructor(
    private activeRoute: ActivatedRoute,
    private router: Router,
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
    
    this.form = new FormGroup({
      name: new FormControl(null, [Validators.required])
    })

    this.activeRoute.params.pipe(
      mergeMap((params: Params) => {
        this.form.disable();

        if(params.id) {
          this.isNew = false;
          return this.categoriesService.getCategoryById(params.id)
        }

        return of(null);
      }),
    ).subscribe((category: Category) => {
      if(category) {
        this.form.patchValue({
          name: category.name
        })
        
        this.category = category;
        this.imagePreview = category.imageSrc;
        MaterialService.updateTextFields();
      }

      this.form.enable();
    },
    error => MaterialService.toast(error.error.message)
    )
  }

  private triggerClick() {
    this.fileInput.nativeElement.click();
  }

  private onFileUpload(e: any) {
    const file = e.target.files[0];
    const reader = new FileReader();

    this.image = file;

    reader.onload = () => this.imagePreview = reader.result;

    reader.readAsDataURL(file);
  }

  private deleteCategory() {
    const decision = confirm(`Вы дейсмтвиетльно хотите удалить категорию ${this.category.name}`);

    if(decision) {
      this.categoriesService.deleteCategory(this.category._id)
        .subscribe(
          (response: Message) => MaterialService.toast(response.message),
          error => MaterialService.toast(error.error.message),
          () => this.router.navigate(['/categories'])
        )
    }
  }

  onSubmit() {
    const { name } = this.form.value;

    this.form.disable();

    let categoryStream$: Observable<Category>;

    if(this.isNew) {
      categoryStream$ = this.categoriesService.createCategory(name, this.image);
    } else {
      const image = this.image ? this.image : null;

      categoryStream$ = this.categoriesService.updateCategory(this.category._id, name, image);
    }

    categoryStream$.subscribe((category: Category) => {
      this.category = category;
      this.form.enable();
      MaterialService.toast('Изменения проведены успешно.')
    },
    error => MaterialService.toast(error.error.message)
    )

  }
}
