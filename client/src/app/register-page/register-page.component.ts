import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { AuthService } from '../shared/services/auth.service';
import { Router, ActivatedRoute } from '@angular/router';
import { MaterialService } from '../shared/classes/material.service';

@Component({
  selector: 'app-register-page',
  templateUrl: './register-page.component.html',
  styleUrls: ['./register-page.component.css']
})
export class RegisterPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  minLength: number = 6;

  constructor(
    private authService: AuthService,
    private router: Router
  ) { }

  ngOnInit() {
    this.form = new FormGroup({
      email: new FormControl(null, [Validators.email, Validators.required], []),
      password: new FormControl(null, [Validators.required, Validators.minLength(this.minLength)], []),
    })
  }

  ngOnDestroy() {
      
  }

  onSubmit() {
    this.form.disable();

    this.authService.register(this.form.value).subscribe(user => {
      this.router.navigate(['/login'], {
        queryParams: {
          registered: true
        }
      })
    },
    error => {
      MaterialService.toast(error.error.message);
    }
    )
  }

}
