import { Component, AfterViewInit, OnDestroy, Output, EventEmitter, Input, ElementRef, ViewChild } from '@angular/core';
import { Filter } from '../../shared/intefaces';
import { MaterialService, MaterialDatePicker } from '../../shared/classes/material.service';

@Component({
  selector: 'app-history-filter',
  templateUrl: './history-filter.component.html',
  styleUrls: ['./history-filter.component.css']
})
export class HistoryFilterComponent implements OnDestroy, AfterViewInit {

  constructor() { }
  
  isValid: boolean = false;
  order: number;
  start: MaterialDatePicker;
  end: MaterialDatePicker;
  @Output('onFilter') onFilter = new EventEmitter<Filter>();
  @ViewChild('start') startRef: ElementRef;
  @ViewChild('end') endRef: ElementRef;


  ngOnDestroy() {
    this.start.destroy();
    this.end.destroy();
  }

  ngAfterViewInit() {
    this.start = MaterialService.initDatePicker(this.startRef, this.validate);
    this.end = MaterialService.initDatePicker(this.endRef, this.validate);
  }

  validate = (): void => {

    const startDate = this.start.date ? new Date(this.start.date).getTime() : undefined;
    const endDate = this.end.date ? new Date(this.end.date).getTime() : undefined;

    if(!startDate || !endDate) { 
       return
    }

    this.isValid = startDate > endDate;

  }

  submitFilter() {
    const filter: Filter = {};

    if(this.start.date) filter.start = this.start.date;
    if(this.end.date) filter.end = this.end.date;
    if(this.order)  filter.order = this.order;

    console.log()

    this.onFilter.emit(filter);
  }

}
