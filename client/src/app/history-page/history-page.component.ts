import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild, ElementRef } from '@angular/core';
import { MaterialService, MaterialInstance } from '../shared/classes/material.service';
import { Subscription } from '../../../node_modules/rxjs';
import { OrdersService } from '../shared/services/orders.service';
import { Order, Filter } from '../shared/intefaces';

const STEP = 2;

@Component({
  selector: 'app-history-page',
  templateUrl: './history-page.component.html',
  styleUrls: ['./history-page.component.css']
})
export class HistoryPageComponent implements OnInit, OnDestroy, AfterViewInit {

  offset: number = 0;
  limit: number = STEP;
  filter: Filter = {};
  
  orders: Order[] = [];
  isFilterVisible: boolean = false;
  isLoad: boolean = false;
  loadingOrders: boolean = false;
  noMoreOrders: boolean = false;
  tooltip: MaterialInstance;
  oSub$: Subscription;
  @ViewChild('tooltip') tooltipRef: ElementRef;

  constructor(
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.fetch();
  }

  ngOnDestroy() {
    if(this.oSub$) this.oSub$.unsubscribe();
    this.tooltip.destroy();
  }

  ngAfterViewInit() {
    this.tooltip = MaterialService.initTooltip(this.tooltipRef);
  }

  fetch() {
    const params = Object.assign({}, this.filter, {offset: this.offset, limit: this.limit})

    this.oSub$ = this.ordersService.fetch(params).subscribe(
      (orders: Order[]) => {
        this.orders = this.orders.concat(orders);
        this.noMoreOrders = orders.length < this.limit || orders[orders.length - 1].order === 1;
        this.isLoad = true;
        this.loadingOrders = false;
      },
      (error) => MaterialService.toast(error.error.message)
    )
  }

  loadMore() {
    this.loadingOrders = true;
    this.offset += STEP;

    this.fetch();
  }

  isFiltered(): boolean {
    return Object.keys(this.filter).length !== 0;
  }
  
  applyFilter(filter: Filter) {
    this.offset = 0;
    this.orders = [];
    this.filter = filter;
    this.loadingOrders = true;
    this.fetch();
  }
}
