import { Component, OnInit, ViewChild, ElementRef, AfterViewInit, OnDestroy } from '@angular/core';
import { Observable } from '../../../node_modules/rxjs';
import { OverViewPage } from '../shared/intefaces';
import { AnalyticsService } from '../shared/services/analytics.service';
import * as moment from 'moment';
import { MaterialInstance, MaterialService } from '../shared/classes/material.service';

@Component({
  selector: 'app-overview-page',
  templateUrl: './overview-page.component.html',
  styleUrls: ['./overview-page.component.css']
})
export class OverviewPageComponent implements OnInit, OnDestroy, AfterViewInit {

  data$: Observable<OverViewPage>;
  data: OverViewPage;
  yesterday: string;
  tapTarget: MaterialInstance;
  @ViewChild('tapTarget') tapTargetRef: ElementRef;

  constructor(private aService: AnalyticsService) { }

  ngOnInit() {
    this.data$ = this.aService.getOverView();
    this.yesterday = moment().add(-1, 'd').format('DD.MM.YYYY');
  }

  ngOnDestroy() {
    this.tapTarget.destroy();
  }

  ngAfterViewInit() {
    this.tapTarget = MaterialService.initTapTarget(this.tapTargetRef);
  }

  viewTapTarget() {
    this.tapTarget.open();
  }

}
