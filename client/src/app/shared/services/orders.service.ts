import { Injectable } from "../../../../node_modules/@angular/core";
import { HttpClient, HttpParams } from "../../../../node_modules/@angular/common/http";
import { Observable } from "../../../../node_modules/rxjs";
import { Order, OrderPosition } from "../intefaces";

@Injectable({
    providedIn: 'root'
})
export class OrdersService {
    constructor(
        private http: HttpClient
    ) {}

    create(orderList: OrderPosition[]): Observable<Order> {
        const newOrder = {
            list: orderList.map((position: OrderPosition) => {
                delete position._id;
                return position;
            })
        }

        return this.http.post<Order>('/api/order', newOrder);
    }

    fetch(params: any = {}): Observable<Order[]> {
        return this.http.get<Order[]>('/api/order', {
            params: new HttpParams({
                fromObject: params
            })
        })
    }
}