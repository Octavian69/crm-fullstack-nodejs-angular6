import { Injectable } from "../../../../node_modules/@angular/core";
import { HttpClient } from "../../../../node_modules/@angular/common/http";
import { Observable } from "../../../../node_modules/rxjs";
import { Position, Message } from "../intefaces";


@Injectable({
    providedIn: 'root'
})
export class PositionService {
    constructor(
        private http: HttpClient
    ) {}

    fetch(categoryId): Observable<Position[]> {
        return this.http.get<Position[]>(`/api/position/${categoryId}`);
    }

    createPosition(position: Position): Observable<Position> {
        return this.http.post<Position>(`/api/position`, position);
    }

    updatePosition(position: Position): Observable<Position> {
        return this.http.patch<Position>(`/api/position/${position._id}`, position);
    }
    
    deletePosition(id: string): Observable<Message> {
        return this.http.delete<Message>(`/api/position/${id}`);
    }
}