import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { Observable } from "rxjs";
import { tap } from "rxjs/operators";
import { User } from "../intefaces";

@Injectable({
    providedIn: 'root'
})
export class AuthService {
    
    token: string;

    constructor(private http: HttpClient) {}

    login(user: User): Observable<{token: string}> {
        return this.http.post<{token: string}>('/api/auth/login', user)
            .pipe(tap(({ token }) => {
                this.setToken(token);
                window.localStorage.setItem('authToken', token);
            }))
    }

    getToken(): string {
        return this.token;
    }

    setToken(token): void {
        this.token = token;
    }

    isAuthenticated(): boolean {
        return !!this.token;
    }

    logout(): void {
        this.token = null;
        window.localStorage.clear();
    }

    register(user: User): Observable<User> {
        return this.http.post<User>('/api/auth/register', user);
    }
}

