import { Injectable } from "../../../../node_modules/@angular/core";
import { HttpClient } from "../../../../node_modules/@angular/common/http";
import { Observable } from "../../../../node_modules/rxjs";
import { OverViewPage, AnalyticsPage } from "../intefaces";

@Injectable({
    providedIn: 'root'
})
export class AnalyticsService {
    constructor(private http: HttpClient) {}

    getOverView(): Observable<OverViewPage> {
        return this.http.get<OverViewPage>('/api/analytics/overview');
    }

    getAnalytics(): Observable<AnalyticsPage> {
        return this.http.get<AnalyticsPage>('/api/analytics/analytics');
    }
}