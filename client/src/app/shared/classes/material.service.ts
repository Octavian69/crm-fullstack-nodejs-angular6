import { ElementRef } from "@angular/core";

declare var M;

export interface MaterialInstance {
    open?: () => void;
    close?: () => void;
    destroy?: () => void;
}

export interface MaterialDatePicker extends MaterialInstance {
    date?: Date;
}

export class MaterialService {

    static toast(message) {
        M.toast({ html: message });
    }

    static initializeFloatingButton(ref: ElementRef) {
        M.FloatingActionButton.init(ref.nativeElement);
    }

    static updateTextFields() {
        M.updateTextFields();
    }

    static initModal(elem: ElementRef): MaterialInstance {
        return M.Modal.init(elem.nativeElement);
    }

    static getInstance(elem: ElementRef) {
      return M.Modal.getInstance(elem.nativeElement);
    }

    static initTooltip(elem: ElementRef): MaterialInstance {
        return M.Tooltip.init(elem.nativeElement)
    }

    static initDatePicker(elem: ElementRef, onClose: () => void): MaterialDatePicker {

        return M.Datepicker.init(elem.nativeElement, {
            format: 'dd.mm.yyyy',
            showClearBtn: true,
            onClose
        })
    }

    static initTapTarget(elem: ElementRef): MaterialInstance {
        return M.TapTarget.init(elem.nativeElement);
    }
}