export interface User {
    email: string,
    password: string
}

export interface Message {
    message: string;
}

export interface Category {
    name: string;
    imageSrc?: string;
    user?: string;
    _id?: string;
}

export interface Position {
    name: string;
    cost: number;
    category?: string;
    user?: string;
    _id?: string;
    quantity?: number;
}

export interface OrderPosition {
    name: string,
    quantity: number,
    cost: number,
    _id?: string
}

export interface Order {
    list: OrderPosition[]
    date?: Date,
    order?: number,
    user?: string,
    _id?: string
}

export interface Filter {
    start?: Date;
    end?: Date;
    order?: number;
}

export interface OverViewPage {
    gain: OverViewPageItem;
    orders: OverViewPageItem;
}

interface OverViewPageItem {
    percent: number;
    compare: number;
    yesterday: number;
    isHigher: boolean;
}

export interface AnalyticsPage {
    average: number;
    chart: AnalyticsChartItem[];
}

interface AnalyticsChartItem {
    label: string;
    gain: number;
    order: number;
}