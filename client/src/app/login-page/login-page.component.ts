import { Component, OnInit, OnDestroy } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Router, ActivatedRoute, Params } from '@angular/router';
import { Subscription } from 'rxjs';

import { AuthService } from '../shared/services/auth.service';
import { MaterialService } from '../shared/classes/material.service';


@Component({
  selector: 'app-login-page',
  templateUrl: './login-page.component.html',
  styleUrls: ['./login-page.component.css']
})
export class LoginPageComponent implements OnInit, OnDestroy {

  form: FormGroup;
  aSub: Subscription;
  minLengthPassword: number = 6;

  constructor(
    private authService: AuthService,
    private router: Router,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
     this.form = new FormGroup({
       email: new FormControl(null, [Validators.required, Validators.email], []),
       password: new FormControl(null, [Validators.required, Validators.minLength(this.minLengthPassword)], []),
     }) 
    
     this.activeRoute.queryParams.subscribe((params: Params) => {
      
      switch('true') {
        case params['registered']: 
             MaterialService.toast('Теперь вы можете войти в систему используя свои данные');
             break;
        case params['accessDenied']:
             MaterialService.toast('Введите свои данные для входа в систему');
             break;
        case params['sessionFailed']:
             MaterialService.toast('Время авторзованной сессии закончено.Войдите в свой кабинет заново.');
             break;
      }
     })
  }
   
  ngOnDestroy() {
    if(this.aSub) this.aSub.unsubscribe();
  }

  onSubmit() {

    this.form.disable();
    
    this.aSub = this.authService.login(this.form.value)
      .subscribe(_ => {
          this.router.navigate(['/overview']);
      },
      error => {
        MaterialService.toast(error.error.message)
        this.form.enable();
      } 
    )
  }

}
