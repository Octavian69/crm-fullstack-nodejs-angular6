import { Component, OnInit, ViewChild, ElementRef, OnDestroy, AfterViewInit } from '@angular/core';
import { Router, RouterEvent, NavigationEnd } from '../../../node_modules/@angular/router';
import { MaterialService, MaterialInstance } from '../shared/classes/material.service';
import { OrderService } from './order.service';
import { OrderPosition, Order } from '../shared/intefaces';
import { OrdersService } from '../shared/services/orders.service';
import { Subscription } from '../../../node_modules/rxjs';


@Component({
  selector: 'app-order-page',
  templateUrl: './order-page.component.html',
  styleUrls: ['./order-page.component.css'],
  providers: [OrderService]
})
export class OrderPageComponent implements OnInit, OnDestroy, AfterViewInit {

  isRoot: boolean;
  isPending: boolean;
  modal: MaterialInstance;
  oSub$: Subscription;
  @ViewChild('modal') modalRef: ElementRef;

  constructor(
    private router: Router,
    private orderService: OrderService,
    private ordersService: OrdersService
  ) { }

  ngOnInit() {
    this.isRoot = this.router.url === '/order';

    this.router.events.subscribe((e: RouterEvent) => {
      if(e instanceof NavigationEnd) {
        this.isRoot = this.router.url === '/order';
      }
    })
    
  }

  ngOnDestroy() {
    if(this.oSub$) this.oSub$.unsubscribe();
    this.modal.destroy();
  }

  ngAfterViewInit() {
    this.modal = MaterialService.initModal(this.modalRef);
  }

  open() {
    this.modal.open();
  }

  cancel() {
    this.modal.close();
  }

  deletePosition(position: OrderPosition) {
    this.orderService.remove(position);
  }

  
  submit() {
   this.isPending = true;

   this.oSub$ = this.ordersService.create(this.orderService.list)
      .subscribe(
        (newOrder: Order) => {
          MaterialService.toast(`Заказ №${newOrder.order} успешно выполнен.`);
          this.orderService.clear();

        },
        (error) => { MaterialService.toast(error.error.message) },
        () => { 
          this.cancel();
          this.isPending = false;
         }
      )
  }
}
