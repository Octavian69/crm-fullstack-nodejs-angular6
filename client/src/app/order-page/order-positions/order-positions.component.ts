import { Component, OnInit } from '@angular/core';
import { PositionService } from '../../shared/services/position.service';
import { OrderService } from '../order.service';
import { Observable } from '../../../../node_modules/rxjs';
import { ActivatedRoute, Params } from '../../../../node_modules/@angular/router';
import { mergeMap, map } from '../../../../node_modules/rxjs/operators';
import { Position } from '../../shared/intefaces';
import { MaterialService } from '../../shared/classes/material.service';

@Component({
  selector: 'app-order-positions',
  templateUrl: './order-positions.component.html',
  styleUrls: ['./order-positions.component.css']
})
export class OrderPositionsComponent implements OnInit {

  positions$: Observable<Position[]>;
  positions: Position[];

  constructor(
    private positionsService: PositionService,
    private orderService: OrderService,
    private activeRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    
    this.positions$ = this.activeRoute.params
      .pipe(mergeMap((params: Params) => {
          return this.positionsService.fetch(params.id);
      }),
      map((positions: Position[]) => {
        return positions.map((position: Position) => {
          position.quantity = 1;
          return position;
        })
      })
    )
  }

  addToOrder(position: Position): void {
    this.orderService.add(position);
    MaterialService.toast(`Позиция ${position.name} добавлена в заказ. (${position.quantity} шт.)`);
  }
}
