import { Component, OnInit } from '@angular/core';
import { CategoriesService } from '../../shared/services/categories.service';
import { Category } from '../../shared/intefaces';
import { Observable } from '../../../../node_modules/rxjs';

@Component({
  selector: 'app-order-categories',
  templateUrl: './order-categories.component.html',
  styleUrls: ['./order-categories.component.css']
})
export class OrderCategoriesComponent implements OnInit {

  categories$: Observable<Category[]> 
  categories: Category[];

  constructor(
    private categoriesService: CategoriesService
  ) { }

  ngOnInit() {
    this.categories$ =  this.categoriesService.fetch();
  }

}
