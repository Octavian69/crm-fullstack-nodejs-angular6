import { OrderPosition, Position } from "../shared/intefaces";


export class OrderService {
    
    list: OrderPosition[] = [];
    price: number = 0;

    add(position: Position) {
        const { name, cost, quantity, _id } = position;
        const positionOrder = { name, cost, quantity, _id };

        const candidateIndex = this.list.findIndex(p => p._id === positionOrder._id);

        if(candidateIndex !== -1) {
            this.list[candidateIndex] = positionOrder;
        } else {
            this.list.push(positionOrder);
        }

        this.calculatePrice();
    }

    remove(position: Position) {
        this.list = this.list.filter(p => p._id !== position._id);
        this.calculatePrice();
    }

    calculatePrice() {
        this.price = this.list.reduce((a, b) => a += (b.cost * b.quantity), 0);
    }

    clear() {
        this.list = [];
        this.price = 0;
    }
}