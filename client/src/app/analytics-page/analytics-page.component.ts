import { Component, AfterViewInit, OnDestroy, ViewChild, ElementRef } from '@angular/core';
import { AnalyticsService } from '../shared/services/analytics.service';
import { Subscription } from '../../../node_modules/rxjs';
import { Chart } from 'chart.js';

import { AnalyticsPage } from '../shared/intefaces';

@Component({
  selector: 'app-analytics-page',
  templateUrl: './analytics-page.component.html',
  styleUrls: ['./analytics-page.component.css']
})
export class AnalyticsPageComponent implements AfterViewInit, OnDestroy {
  
  average: number = 0;
  pending: boolean = true;
  aSub: Subscription;

  @ViewChild('gain') gainRef: ElementRef;
  @ViewChild('order') orderRef: ElementRef;

  constructor(
    private aService: AnalyticsService
  ) { }

  ngAfterViewInit() {

    const gainConfig: any = {
      label: 'Выручка',
      color: 'rgb(255, 99, 132)'
    };

    const orderConfig: any = {
      label: 'Заказы',
      color: 'rgb(54, 162, 235)'
    };

    this.aSub = this.aService.getAnalytics().subscribe((data: AnalyticsPage) => {
      this.average = data.average;
      this.pending = false;

      gainConfig.labels = data.chart.map(item => item.label);
      gainConfig.data = data.chart.map(item => item.gain);

      orderConfig.labels = data.chart.map(item => item.label);
      orderConfig.data = data.chart.map(item => item.order);
      
      // Gain - пример
      gainConfig.labels.push('26.07.2018')
      gainConfig.labels.push('27.07.2018')
      gainConfig.data.push(1500)  
      gainConfig.data.push(600) 
      // Gain - пример

      // Order - пример
      orderConfig.labels.push('26.07.2018')
      orderConfig.labels.push('27.07.2018')
      orderConfig.data.push(4)  
      orderConfig.data.push(3) 
      // Order - пример

      const gainCtx = this.gainRef.nativeElement.getContext('2d');
      const orderCtx = this.orderRef.nativeElement.getContext('2d');

      gainCtx.canvas.height = '300px';
      orderCtx.canvas.height = '300px';
      

      new Chart(gainCtx, createChartConfig(gainConfig));
      new Chart(orderCtx, createChartConfig(orderConfig));
    })
  }

  ngOnDestroy() {
    if(this.aSub) this.aSub.unsubscribe();
  }
}

function createChartConfig({ label, color, labels, data }): any {
  return {
    type: 'line',
    options: { responsive: true },
    data: {
      labels,
      datasets: [
        {
          label, data,
          borderColor: color,
          steppedLine: false,
          fill: false
        }
      ]
    }
  }
}

