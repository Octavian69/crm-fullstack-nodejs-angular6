const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const OrderSchema = new Schema({
    date: {
        type: Date,
        default: Date.now
    },
    order: {
        type: Number,
        required: true
    },
    list: [
        {
            name: {
                type: String,
            },
            quantity: {
                type: Number,
            },
            cost: {
                type: Number,
            }
        }
    ],
    user: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})

module.exports = mongoose.model('order', OrderSchema);
