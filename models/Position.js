const mongoose = require('mongoose');

const Schema = mongoose.Schema;


const PositionSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    cost: {
        type: Number,
        required: true
    },
    category: {
        refs: 'categories',
        type: Schema.Types.ObjectId
    },
    user: {
        refs: 'users',
        type: Schema.Types.ObjectId
    }
})


module.exports = mongoose.model('position', PositionSchema);