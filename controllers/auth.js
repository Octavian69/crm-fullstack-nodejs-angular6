const bcrypt = require('bcryptjs');
const jwt = require('jsonwebtoken');

const User = require('../models/User');
const keys = require('../config/keys');
const errorHandler = require('../utils/errorHandler');


async function login(req, res) {
    const candidate = await User.findOne({ email: req.body.email });

    if(candidate) {

        const validPassword = bcrypt.compareSync(req.body.password, candidate.password);

        if(validPassword) {
            
            const token = jwt.sign({
                email: candidate.email,
                userId: candidate._id
            }, keys.token, { expiresIn: 60 * 60 });

            res.status(200).json({ token: `Bearer ${token}` });
            

        } else {
            res.status(401).json({ message: 'Неккорректный пароль.Попробуйте еще раз.' });
        }

    } else {
        res.status(404).json({ message: 'Пользователь с таким email не сущестует.' })
    }
}

async function register(req, res) {
    const candidate = await User.findOne({ email: req.body.email });

    if(candidate) {
        res.status(409).json({ message: 'Пользователь с таким email адресом уже существует.Попробуйте другой.' })
    } else {

        const salt = bcrypt.genSaltSync(10);
        const password = bcrypt.hashSync(req.body.password, salt);

        const user = new User({
            email: req.body.email,
            password
        })

        try {

           await user.save();

           res.status(201).json(user);

        } catch(e) {
            errorHandler(res, e);
        }
    }
}


const controller = { 
    login,
    register
}

module.exports = controller;