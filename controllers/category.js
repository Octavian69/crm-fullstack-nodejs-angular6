const Category = require('../models/Category');
const Position = require('../models/Position');
const errorHandler = require('../utils/errorHandler');

async function getAll(req, res) {
    try {
        const categories = await Category.find({ user: req.user.id });

        res.status(200).json(categories);
    } catch(e) {
        errorHandler(res, e.message)
    }
}

async function getCategoryById(req, res) {
    try {

        const category = await Category.findById(req.params.id);

        res.status(200).json(category);

    } catch(e) {
        errorHandler(res, e)
    }
}

async function remove(req, res) {
    try {
        await Category.findByIdAndRemove(req.params.id);
        await Position.remove({ category: req.param.category });
        res.status(200).json({message: 'Категория успешно удалена'})
    } catch(e) {
        errorHandler(res, e)
    } 
}

async function create(req, res) {
    const category = new Category({
        name: req.body.name,
        imageSrc: req.file ? req.file.path : '',
        user: req.user.id
    })

    try {

        await category.save();

        res.status(201).json(category);

    } catch(e) {
        errorHandler(res, e)
    } 
}

async function update(req, res) {

    const updated = {
        name: req.body.name
    }

    if(req.file) updated.imageSrc = req.file.path;

    try {

        const category = await Category.findByIdAndUpdate(
            req.params.id,
            {$set: updated},
            {new: true}
        )

        console.log(category)
    
        res.status(200).json(category);
        
    } catch(e) {
        errorHandler(res, e)
    }
}

const category = {
    getAll,
    getCategoryById,
    remove,
    create,
    update
}

module.exports = category;

