const moment = require('moment');

const Order = require('../models/Order');
const errorHandler = require('../utils/errorHandler')

async function overview(req, res) {
    try {
        const allOrders = await Order.find({user: req.user.id}).sort({date: 1});

        //Объект всех дней с заказами
        const ordersMap = getOrdersMap(allOrders);

        // Массив с заказами вчера
        const yesterdayOrders = ordersMap[moment().add(-1, 'd').format('DD.MM.YYYY')] || [];
        
        // Количество заказов
        const totalOrdersNumber = allOrders.length

        // Количество заказов вчера
        const yesterdayOrdersNumber = yesterdayOrders.length;

        // Количество дней всего 
        const daysNumber = Object.keys(ordersMap).length;

        // Заказов в день
        const ordersPerDay = (totalOrdersNumber / daysNumber ).toFixed();

        // Процент для количества заказов ((заказов вчера / заказов в день) - 1) * 100
        const ordersPercent = (((yesterdayOrdersNumber / daysNumber) - 1) * 100).toFixed(2);

        // Общая выручка 
        const totalGain = calculatePrice(allOrders);

        // Выручка в день
        const gainPerDay = totalGain / daysNumber;

        // Выручка за вчера
        const gainPerYesterday = calculatePrice(yesterdayOrders);

        // Процент выручки
        const gainPercent = (((gainPerYesterday / gainPerDay) - 1) * 100).toFixed(2);

        // Сравнение выручки
        const compareGain = (gainPerYesterday - gainPerDay).toFixed(2);

        // Сравнение количества заказов
        const compareOrders = (yesterdayOrdersNumber - ordersPerDay).toFixed(2);

        res.status(200).json({
            gain: {
                percent: Math.abs(+gainPercent),
                compare: Math.abs(+compareGain),
                yesterday: +gainPerYesterday,
                isHigher: +gainPercent > 0
            },
            orders: {
                percent: Math.abs(+ordersPercent),
                compare: Math.abs(+compareOrders),
                yesterday: +yesterdayOrdersNumber,
                isHigher: +ordersPercent > 0
            }
        })

    } catch(error) {
        errorHandler(res, error);
    }
}

async function analytics(req, res) {
    try {

        const allOrders = await Order.find({user: req.user.id}).sort({date: 1});

        const ordersMap = getOrdersMap(allOrders);

        const average = +(calculatePrice(allOrders) / Object.keys(ordersMap).length).toFixed(2);

        const chart = Object.keys(ordersMap).map(label => {
            const gain = calculatePrice(ordersMap[label]);
            const order = ordersMap[label].length;

            return { label, order, gain }
        })

        res.status(200).json({ average, chart});

    } catch(e) {
        errorHandler(res, error);
    }

    
}

const controller = {
    overview,
    analytics
}

module.exports = controller;

function getOrdersMap(orders) {
    const daysOrders = {};

    orders.forEach(order => {
        let day = moment(order.date).format('DD.MM.YYYY');
        

        if(day === moment().format('DD.MM.YYYY')) return;
        
        if(!daysOrders[day]) {
            daysOrders[day] = [];

        } 

        daysOrders[day].push(order);
    })

   

    return daysOrders;
}

function calculatePrice(orders) {
    let price = 0;

    orders.forEach(order => {

        let dayPrice = order.list.reduce((a, b) => a += (b.cost * b.quantity), 0);

        price += dayPrice;
    })

    return price;
}


// averge chart  