const Position = require('../models/Position');
const errorHandler = require('../utils/errorHandler');


async function getByGategoryId(req, res) {

    try {
        const positions = await Position.find({
            category: req.params.categoryId,
            user: req.user.id
        });

        res.status(200).json(positions);



    } catch(e) {
        errorHandler(res, e);
    }

}

async function create(req, res) {

try {
        const position = new Position({
            name: req.body.name,
            cost: req.body.cost,
            category: req.body.category,
            user: req.user.id
        });

        await position.save();

        res.status(201).json(position)

    } catch(e) {
        errorHandler(res, e);
    }
}

async function update(req, res) {
    try {
       const position = await Position.findByIdAndUpdate(req.params.id, {$set: req.body}, { new: true })

       res.status(200).json(position);
    } catch(e) {
        errorHandler(res, e);
    }
}

async function remove(req, res) {
    try {

        const position = await Position.findByIdAndRemove(req.params.id)

        res.status(200).json({ message: 'Позиция была удалена.' })

    } catch(e) {
        errorHandler(res, e);
    }
}

const controller = {
    getByGategoryId,
    create,
    update,
    remove
}

module.exports = controller;