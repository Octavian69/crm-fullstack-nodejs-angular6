const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require('cors');
const morgan = require('morgan');
const passport = require('passport');
const path = require('path')

const keys = require('./config/keys');
const authRoutes = require('./routes/auth');
const analyticsRoutes = require('./routes/analytics');
const categoryRoutes = require('./routes/category');
const orderRoutes = require('./routes/order');
const positionRoutes = require('./routes/position');

const app = express();

mongoose.Promise = global.Promise;


mongoose.connect(keys.mongoURI, {
        useNewUrlParser: true,
        reconnectTries: Number.MAX_VALUE,
        reconnectInterval: 1000 
    })
    .then(_ => console.log('MongoDB get started.'))
    .catch(e => console.log(e));

app.use(passport.initialize());

require('./middleware/passport')(passport);


app.use(morgan('dev'));

app.use('/uploads', express.static('./uploads'));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(cors());


app.use('/api/auth', authRoutes);

app.use('/api/analytics', analyticsRoutes);

app.use('/api/category', categoryRoutes);

app.use('/api/order', orderRoutes);

app.use('/api/position', positionRoutes);


if(process.env.NODE_ENV === 'production') {
    app.use(express.static('client/dist/client'));

    app.get('*', (req, res) => {
        res.sendFile(
            path.resolve(
                __dirname, 'client', 'dist', 'client', 'index.html'
            )
        )
    })
}


module.exports = app;


